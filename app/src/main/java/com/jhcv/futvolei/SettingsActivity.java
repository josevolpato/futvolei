/**
 * SettingsActivity.java
 *
 * MIT License.
 *
 * Copyright (c) 2016 Jose Volpato
 */
package com.jhcv.futvolei;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class SettingsActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTitle(R.string.SettingsActivity_title);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

}
