/**
 * SettingsFragment.java
 *
 * MIT License.
 *
 * Copyright (c) 2016 Jose Volpato
 */
package com.jhcv.futvolei;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;


public class SettingsFragment extends PreferenceFragment
{
    private ListPreference notificationTime;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        this.notificationTime = (ListPreference) findPreference("notification_time");
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Define o valor padrao da preferencia de tempo de notificacao
        if (this.notificationTime.getValue() == null)
        {
            this.notificationTime.setValue("15");
        }

        // Define o valor inicial do sumario da preferencia de tempo de notificacao
        switch (this.notificationTime.getValue())
        {
            case "0":
                this.notificationTime.setSummary(getString(R.string.SettingsActivity_notificationNoTime));
                break;

            case "1":
                this.notificationTime.setSummary(getString(R.string.SettingsActivity_notificationOneMinute));
                break;

            case "60":
                this.notificationTime.setSummary(getString(R.string.SettingsActivity_notificationOneHour));
                break;

            default:
                this.notificationTime.setSummary(this.notificationTime.getValue() + " " + getString(R.string.SettingsActivity_notificationGeneric));
                break;
        }

        // Muda o sumario da preferencia de tempo de notificacao para a opcao escolhida
        this.notificationTime.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue)
            {
                String value = newValue.toString();

                switch (value)
                {
                    case "0":
                        preference.setSummary(getString(R.string.SettingsActivity_notificationNoTime));
                        break;

                    case "1":
                        preference.setSummary(getString(R.string.SettingsActivity_notificationOneMinute));
                        break;

                    case "60":
                        preference.setSummary(getString(R.string.SettingsActivity_notificationOneHour));
                        break;

                    default:
                        preference.setSummary(value + " " + getString(R.string.SettingsActivity_notificationGeneric));
                        break;
                }

                return true;
            }

        });

    }
}
