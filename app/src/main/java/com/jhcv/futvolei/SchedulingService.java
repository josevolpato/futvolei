/**
 * SchedulingService.java
 *
 * MIT License.
 *
 * Copyright (c) 2016 Jose Volpato
 */
package com.jhcv.futvolei;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;


public class SchedulingService extends IntentService
{
    private final int NOTIFICATION_ID = 44;


    public SchedulingService()
    {
        super("SchedulingService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        sendNotification();
        AlarmReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification()
    {
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_football)
                .setContentTitle("Titulo")
                .setContentText("Mensagem!");
        builder.setSound(alarmSound);
        builder.setContentIntent(contentIntent);
        builder.setAutoCancel(true);
        builder.setLights(ContextCompat.getColor(this, R.color.colorFootball), 500, 500);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }


}
