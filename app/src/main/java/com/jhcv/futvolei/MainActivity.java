/**
 * MainActivity.java
 *
 * MIT License.
 *
 * Copyright (c) 2016 Jose Volpato
 */
package com.jhcv.futvolei;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;


public class MainActivity extends AppCompatActivity
{
    private LinearLayout llBackground;
    private LinearLayout llDay;
    private TextView lblDayMessage;
    private ImageView ivSportLogo;
    private TextView lblDaySport;
    private TableLayout tableNextDays;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.llBackground = (LinearLayout) findViewById(R.id.llBackground);
        this.llDay = (LinearLayout) findViewById(R.id.llDay);
        this.lblDayMessage = (TextView) findViewById(R.id.lblDayMessage);
        this.ivSportLogo = (ImageView) findViewById(R.id.ivSportLogo);
        this.lblDaySport = (TextView) findViewById(R.id.lblDaySport);
        this.tableNextDays = (TableLayout) findViewById(R.id.tableNextDays);

        ViewCompat.setElevation(this.llDay, 8);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Verifica as configuracoes
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean prefFollowFootball = preferences.getBoolean("follow_football", true);
        boolean prefFollowVolley = preferences.getBoolean("follow_volley", true);
        String prefNotificationTimeString = preferences.getString("notification_time", "15");
        int prefNotificationTime = Integer.parseInt(prefNotificationTimeString);

        // Define a mensagem do dia
        Calendar nextDay = Calendar.getInstance();
        StringBuilder dayMessage = new StringBuilder();
        dayMessage.append(getString(R.string.MainActivity_lblDayMessage_Today));
        if (!isWednesday(nextDay))
        {
            nextDay = findNextWednesday();
            dayMessage.setLength(0);
            dayMessage.append(getString(R.string.MainActivity_lblDayMessage_NextWednesday1))
                    .append(" ")
                    .append(DateFormat.getDateInstance().format(nextDay.getTime()))
                    .append(" ")
                    .append(getString(R.string.MainActivity_lblDayMessage_NextWednesday2));
        }
        this.lblDayMessage.setText(dayMessage);

        // Verifica se a proxima quarta-feira e do futebol ou do volei
        if (isFootball(nextDay))
        {
            this.llBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.colorFootball));
            this.ivSportLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.logo_football));
            this.lblDaySport.setText(getText(R.string.MainActivity_lblSportNameFootball));
            this.llDay.setBackgroundColor(ContextCompat.getColor(this, R.color.colorFootball));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                Window window = this.getWindow();
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorFootballStatusBar));
                ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(
                        getString(R.string.app_name),
                        null,
                        ContextCompat.getColor(this, R.color.colorFootballStatusBar));
                this.setTaskDescription(taskDescription);
            }
        }
        else
        {
            this.llBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.colorVolley));
            this.ivSportLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.logo_volley));
            this.lblDaySport.setText(getText(R.string.MainActivity_lblSportNameVolleyball));
            this.llDay.setBackgroundColor(ContextCompat.getColor(this, R.color.colorVolley));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                Window window = this.getWindow();
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorVolleyStatusBar));
                ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(
                        getString(R.string.app_name),
                        null,
                        ContextCompat.getColor(this, R.color.colorVolleyStatusBar));
                this.setTaskDescription(taskDescription);
            }
        }

        // Popula a tabela de proximas atividades
        this.tableNextDays.removeAllViews();
        if (!prefFollowFootball && !prefFollowVolley)
        {
            TextView lblNotFollowingActivities = new TextView(this);
            lblNotFollowingActivities.setTextSize(20);
            lblNotFollowingActivities.setGravity(Gravity.CENTER);
            lblNotFollowingActivities.setTextColor(ContextCompat.getColor(this, R.color.colorFont));
            lblNotFollowingActivities.setText(getString(R.string.MainActivity_lblNotFollowingActivities));
            this.tableNextDays.addView(lblNotFollowingActivities);
        }
        else
        {
            Calendar nextActivityDate = getNextActivityDate(Calendar.getInstance());
            int i = 0;
            while (i < 6)
            {
                switch (nextActivityDate.get(Calendar.DAY_OF_WEEK))
                {
                    case Calendar.TUESDAY:
                        if (prefFollowVolley)
                        {
                            addActivityEntry(nextActivityDate);
                            i++;
                        }
                        nextActivityDate.add(Calendar.DAY_OF_MONTH, 1);
                    break;

                    case Calendar.WEDNESDAY:
                        if (isFootball(nextActivityDate))
                        {
                            if (prefFollowFootball)
                            {
                                addActivityEntry(nextActivityDate);
                                i++;
                            }
                        }
                        else
                        {
                            if (prefFollowVolley)
                            {
                                addActivityEntry(nextActivityDate);
                                i++;
                            }

                        }
                        nextActivityDate.add(Calendar.DAY_OF_MONTH, 1);
                    break;

                    case Calendar.THURSDAY:
                        if (prefFollowFootball)
                        {
                            addActivityEntry(nextActivityDate);
                            i++;
                        }
                        nextActivityDate.add(Calendar.DAY_OF_MONTH, 5);
                    break;
                }
            }
        }

        // Alarme de notificacao de atividade
        //AlarmReceiver alarmReceiver = new AlarmReceiver();
        //alarmReceiver.setAlarm(this);

    }

    /**
     * Abre a tela de configuracoes do aplicativo.
     */
    public void openSettingsActivity(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        this.startActivity(intent);
    }

    /**
     * Verifica se o dia cai em uma quarta-feira.
     *
     * @param day - Dia a ser verificado.
     * @return true - O dia cai em uma quarta-feira.
     * false - O dia nao cai em uma quarta-feira.
     */
    private boolean isWednesday(Calendar day)
    {
        return day.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY;
    }

    /**
     * Verifica se o dia e do futebol.
     *
     * @param day - Dia a ser verificado.
     * @return true - O dia e do futebol.
     * false - O dia e do volei.
     */
    private boolean isFootball(Calendar day)
    {
        if (day.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
        {
            return false;
        }

        if (day.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
        {
            return true;
        }

        // Dia de referencia, 06/04/2016, futebol
        final Calendar referenceDay = Calendar.getInstance();
        referenceDay.set(Calendar.DAY_OF_MONTH, 6);
        referenceDay.set(Calendar.MONTH, 3);
        referenceDay.set(Calendar.YEAR, 2016);
        int distance = day.get(Calendar.DAY_OF_YEAR) - referenceDay.get(Calendar.DAY_OF_YEAR);
        return distance % 14 == 0;
    }

    /**
     * Procura a proxima quarta-feira, partindo da data atual.
     *
     * @return Calendar contendo a data da proxima quarta-feira.
     */
    private Calendar findNextWednesday()
    {
        return findNextWednesday(Calendar.getInstance());
    }

    /**
     * Procura a proxima quarta-feira, partindo da data passada pelo parametro.
     *
     * @return Calendar contendo a data da proxima quarta-feira.
     */
    private Calendar findNextWednesday(Calendar date)
    {
        while (date.get(Calendar.DAY_OF_WEEK) != Calendar.WEDNESDAY)
        {
            date.add(Calendar.DAY_OF_WEEK, 1);
        }
        return date;
    }

    /**
     * Converte uma medida de Dp para pixels.
     *
     * @param dp - Medida em Dp a ser convertida.
     * @return Medida convertida em pixels.
     */
    private int convertDptoPixel(int dp)
    {
        Resources r = getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    /**
     * Retorna a data da proxima atividade, seja ela qual for.
     *
     * @param date - Data de inicio da procura.
     * @return Data da proxima atividade.
     */
    private Calendar getNextActivityDate(Calendar date)
    {
        switch (date.get(Calendar.DAY_OF_WEEK))
        {
            case Calendar.TUESDAY:
            case Calendar.WEDNESDAY:
            case Calendar.THURSDAY:
            return date;

            default:
                while (date.get(Calendar.DAY_OF_WEEK) != Calendar.TUESDAY)
                {
                    date.add(Calendar.DAY_OF_MONTH, 1);
                }
            return date;
        }
    }

    /**
     * Adiciona um item de atividade na tabela de proximas atividades.
     *
     * @param date - Data da atividade a ser adicionada na tabela de proximas atividades.
     */
    private void addActivityEntry(Calendar date)
    {
        TextView lblDate = new TextView(this);
        LinearLayout.LayoutParams paramsLblDate = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLblDate.leftMargin = convertDptoPixel(5);
        lblDate.setLayoutParams(paramsLblDate);
        lblDate.setTextSize(25);
        lblDate.setTextColor(ContextCompat.getColor(this, R.color.colorFont));
        lblDate.setText(DateFormat.getDateInstance().format(date.getTime()));

        TextView lblSport = new TextView(this);
        lblSport.setTextColor(ContextCompat.getColor(this, R.color.colorFont));
        LinearLayout.LayoutParams paramsLblSport = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLblSport.leftMargin = convertDptoPixel(5);
        lblSport.setLayoutParams(paramsLblSport);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setGravity(Gravity.CENTER_VERTICAL);
        ll.addView(lblDate);
        ll.addView(lblSport);

        TableLayout tableLayout = new TableLayout(this);
        tableLayout.addView(ll);
        ll.getLayoutParams().height = convertDptoPixel(70);

        String dayOfWeek = "";
        switch (date.get(Calendar.DAY_OF_WEEK))
        {
            case Calendar.TUESDAY:
                dayOfWeek = getString(R.string.tuesday);
            break;

            case Calendar.WEDNESDAY:
                dayOfWeek = getString(R.string.wednesday);
            break;

            case Calendar.THURSDAY:
                dayOfWeek = getString(R.string.thursday);
            break;
        }

        if (isFootball(date))
        {
            ll.setBackgroundColor(ContextCompat.getColor(this, R.color.colorFootball));
            String placeHolder = dayOfWeek + ", " + getString(R.string.MainActivity_lblSportNameFootball);
            lblSport.setText(placeHolder);
        }
        else
        {
            ll.setBackgroundColor(ContextCompat.getColor(this, R.color.colorVolley));
            String placeHolder = dayOfWeek + ", " + getString(R.string.MainActivity_lblSportNameVolleyball);
            lblSport.setText(placeHolder);
        }

        this.tableNextDays.addView(tableLayout);
    }

}
